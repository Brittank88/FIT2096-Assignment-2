# Intro

Hello! I hope you like this submission, because I put a lot of effort into it! Ignore how the map looks low-effort, this is intentional (for a bit of fun!).

Your "Player Goal" (which does nothing as we were never told to make it do anything, and it doesn't appear in the rubric), is the swirling white vertical plane with the blue text over it that says "Does this mean I get full marks now". Yes, it really says that (in jest, though, I can be hopeful!).

Below I've outlined how I accomplished every requirement of the assignment brief. I've also hightlighted any extra features I added that may be of interest.

Please feel free to explore the project further yourself, with any time you have left allocated to marking me! Maybe there's something I have forgotten to include here, in that case, there's a chance you'll see it!

---

# Assignment Brief Requirements

## Respawn On Collide With Agent:
	- The visible mesh of the agent's body has been configured to generate overlaps only, when in contact with pawns.
		- This was achieved by setting the ECollisionResponse type to ECR_Overlap for the ECollisionChannel::ECC_Pawn collision channel of the mesh.\
			- See AgentActor.cpp, line 99.

	- A handler function for occurring overlaps (AAgentActor::BeginOverlap) is registered and called whenever an overlap is triggered.
		- See AgentActor.cpp, lines 100, 152 and AgentActor.h, line 64.

	- This handler function checks if the other actor the agent overlapped with is the player, and causes the player to respawn if so.
		- AgentActor.cpp, lineline 161 for player check
		- See AgentActor.cpp, lines 163, 165, 167, 169, 171 for manually respawning player.
			- NOTE: I would have used AGameModeBase::ResetPlayer(), however, it refused to work... Player would remain in place, with only the pitch being reset (weirdly...).
				- I hypothesise that the function is failing to determine the actual spawnpoint of the player.

## Agent Has StarterContent Model With ArrowComponent For Direction:
	- I start by creating the RootComponent (AgentActor.cpp, line 42).
	
	- Next, I find the NarrowCapsule mesh from the StarterContent, and assign it to a UStaticMeshComponent (VisibleComponentBody), which is assigned the RootComponent as a parent.
		- See AgentActor.cpp, lines 49-51 for finding the mesh.
		- See AgentActor.cpp, line 52 for assigning the mesh to VisibleComponentBody (defined in AgentActor.h at line 27).
		- See AgentActor.cpp, line 46 for attaching VisibleComponentBody to the RootComponent.
		
	- Next, I create and assign to the RootComponent a default subobject for my arrow component (VisibleComponentArrow, defined in AgentActor.h at line 31).
		- See AgentActor.cpp, line 62 for default subobject creation.
		- See AgentActor.cpp, line 64 for attaching VisibleComponentArrow to the RootComponent.
		- By default, UArrowComponent instances are hidden in-game. I make sure to toggle this to true instead of false.
			- See AgentActor.cpp, line 63 for the toggle.
		- By default, the VisibleComponentArrow will protrude from the bottom of the agent, where the RootComponent origin is located. I change it to protrude from halfway up the agent.
			- See AgentActor.cpp, lines 67-69 for this location adjustment.
			
	- When the agent rotates towards waypoints, the whole model (including the arrow!) will rotate too, meaning this is handled automatically when we rotate towards waypoints.
	
## Agent Waypoint Navigation:
	- "Your agents must use a set of waypoints to navigate their way through the game level."
	+ "Waypoints will be hard coded and stored in an array."
		- See AgentActor.cpp, lines 13-33 for hardcoded waypoint arrays.
			- Waypoints are organised into TArray<FVector> entries, representing singular circular paths.
				- Each path or "Track" is chosen via an index that is a property of the agent.
					- See AgentActor.h, line 43.
				- The next waypoint from the track is chosen via an index that is a read-only property of the agent that is incremented after each waypoint has been arrived at.
					- See AgentActor.h, line 53.
					- NOTE: We actually start targeting the second waypoint, as we are already at the first.
						- See AgentActor.cpp, line 74.
		- See AgentActor.cpp, line 109 for getting the waypoint to navigate to next from the hardcoded arrays.
		
	- "Each waypoint contains a location (x,y,z) in the game level."
		- See AgentActor.cpp, lines 13-33 for hardcoded waypoint arrays.
			- Each waypoint is an FVector containing coordinates in world space for the agent to move to.
			
	- "The agent should spawn at the first waypoint, rotate to face the next waypoint and move to that waypoint."
	+ "When the agent reaches the waypoint it should rotate to face the next waypoint and move to it."
		- Spawn At First Waypoint:
			- Agents are spawned via a spawner class (SpawnerActor). These spawners are placed at exactly the position of the first waypoint of the spawned agent's waypoint track.
		
		- Rotate To Face + Moving To Waypoints:
			- Whether an agent is in the 'rotating towards' or 'moving towards' phase is decided by a boolean value (bIsTurning, see AgentActor.h, line 56).
				- The agent will begin by turning towards the waypoint following the one it spawned at (waypoint #2).
					- See AgentActor.cpp, line 75 for defaulting to turning first.
					- See AgentActor.cpp, lines 111-132 for turning during the turning phase.
				- The agent will then move towards the waypoint once it is facing it.
					- See AgentActor.cpp, lines 131 for switching the state to 'moving towards' when the angle is close enough to be considered 'facing'.
					- See AgentActor.cpp, lines 133-137 for moving towards the next waypoint during the movement phase.
					- Once we are close enough to the waypoint, we target the next waypoint and return to the turning phase.
						- See AgentActor.cpp, lines 140-147.
						
	- "The agents should patrol along the path until the end of the game."
	+ "Your paths should form a loop that your agents can follow repeatedly."
		- Once the agent reaches the last waypoint in the track, it targets the first waypoint in the track again. It will do this forever.
			- See AgentActor.cpp, line 143 for 'circular indexing' of the waypoints.
			
	- "You will have at least 2 paths your agents will patrol in the level."
		- I have implemented four separate tracks.
			- See:
				- Room with water-textured and glass-textured walls.
				- Room with large torus.
				- Room with rock.
				- Room with pyramid.
				
## Agent Spawning:
	- NOTE: If the SpawnerActor is spawning Agents, it also has the opportunity to set their properties as they spawn.
		- See SpawnerActor.h, lines 49-55 for parameters that are set on the spawned agents.
		- See SpawnerActor.cpp, lines 73-86 for deferred spawning allowing for setting properties on the spawned actor.

	- "How often the Agent spawns."
		- The SpawnerActor has a paramater that controls the duration between spawns (see SpawnerActor.h, line 35).
			- A countdown value is decremented by the DeltaTime, every tick. When it reaches zero, a spawn occurs and the countdown is reset.
				- See SpawnerActor.h, line 63 for the read-only countdown variable.
				- See SpawnerActor.cpp, line 68 for the check to see if we can spawn again.
				- See SpawnerActor.cpp, line 93 for the countdown reset.
				
	- "The maximum number of Agents spawned."
		- The SpawnerActor has a parameter that controls the maximum count of spawns (see SpawnerActor.h, line 43).
			- A spawn count value is incremented for each spawn, and if it reaches the maximum spawn count, the spawner is destroyed.
				- See SpawnerActor.h, line 74 for the read-only spawn count variable.
				- See SpawnerActor.cpp, line 89 for the spawn count increment.
				- See SpawnerActor.cpp, line 93 for the destruction of the spawner when it has reached its spawning cap.
				
## Speed & Size:
	- "You must have two different types of agents denoted by different coloured Materials."
		- The hue of an agent's material is dynamically determined by its size (AKA type).
			- See "/Game/Assignment2/Materials/Unstable" in the Content Browser for the tinting parameters being set from the AgentActor class.
			- See AgentActor.cpp, line 90 for converting the used material to a UMaterialInstanceDynamic instance that can accept parameter adjustments.
			- See AgentActor.cpp, lines 91-96 for remapping the size range to the hue range ((1,3)->(0,1)), and applying the hue adjustment using the tint parameters.
			
	- "They should travel at different speeds."
	+ "Each type of agent should have a different scale value with larger Agents moving slower, while smaller agents moving faster."
		- Agents have a public BaseSpeed parameter (see AgentActor.h, line 39) that determines their 'pre-sized' speed.
			- This is adjusted by the Size parameter of the agent (see AgentActor.h, line 35), to become the FinalSpeed read-only parameter (see AgentActor.h, line 50).
				- See AgentActor.cpp, line 87 for the factoring in of size in determining the final agent speed, via an inversely proportionate relationship.
		- When an agent is moving, the amount it moves or rotates per tick is multiplied by FinalSpeed (see AgentActor.cpp, lines 127 & 136).
		
	- "Your application must implement the following controls:"
		- "• ‘q’ – Start"
		+ "o Starts the game"
			- Starting the game after pausing it is accomplished via UGameplayStatics::SetGamePaused with the boolean set to false (see Assignment2Character.cpp, line 316).
		
		- "• ‘p’ – Pause"
		+ "o Pauses the game"
			- Pausing the game is accomplished via UGameplayStatics::SetGamePaused with the boolean set to true (see Assignment2Character.cpp, line 321).
		
		- "• ‘r’ – Reset"
		+ "o Resets the game back to its initial state so it can be run again"
			- Restarting the game is accomplished via AGameMode::RestartGame().
				- In order to retrieve the current gamemode as AGameMode and not AGameModeBase (which does not have the RestartGame() function), I modified the Assignment2GameMode class.
					- See Assignment2GameMode.h, line 10 and Assignment2GameMode.cpp, line 9 for the inheritence modification.
					- See Assignment2Character.cpp, line 326 for the retrieval of the AGameMode instance.
						- NOTE: GetWorld()->GetAuthGameMode() always returns AGameModeBase, however, because of the modification we made, we can cast it back to AGameMode without error.

		- NOTE: All controls are registered in the same way.
			- The controls are defined in the project settings, under "Input".
			- The functions handling the control inputs (see Assignment2Character.cpp, lines 314-327) are bound to the PlayerInputComponent (see Assignment2Character.cpp, lines 146-150).
				- NOTE: Start and Restart are specifically configured to accept input even when the game is paused (".bExecuteWhenPaused = true;"). Pausing would softlock, otherwise.

# Extra Functionality

	- Projectiles now give off light, making the game not impossible to play.
		- Achieved via an attached UPointLightComponent.

	- Projectiles no longer collide with Pawns (including the player, which inherits from APawn).
		- This was done by modifying the Projectile collision channel in the project settings (under "Collision").
		- This is to prevent annoyances when trying to walk past projectiles in enclosed spaces.
		
	- Crosshair disappears and is replaced with the text "PAUSED" when the game is paused.
		- Achieved by editing Assignment2HUD::DrawHUD() to only draw one or the other depending on the game pause state.
			- Game pause state is retrieved using UGameplayStatics::IsGamePaused(PlayerOwner->GetWorld()).
				- PlayerOwner is the 'owner' of the HUD.
			- I used an engine font as the font for the "PAUSED" text, so it should always be available.
				
	- I gave that random bench I placed (why the hell is it called a couch in the model name) a collision model because it didn't have one (nice one, StarterContent).
		- You can view this in the pyramid room, though, why it would have any importance to the point of specifically seeking it out is beyond me. :P
		
	- After staying up till 6AM finishing off this assignment so I can stop worrying about it, the most prominent feature implemented is a piece of my soul, which has been ripped away from me and will stay embedded in this project forever.Assignment2
