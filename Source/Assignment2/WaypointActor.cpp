// Fill out your copyright notice in the Description page of Project Settings.


#include "WaypointActor.h"

// Sets default values
AWaypointActor::AWaypointActor()
{
	// Waypoints simply need to exist at a location, and don't need to be ticked.
	PrimaryActorTick.bCanEverTick = false;

}

// Called when the game starts or when spawned
void AWaypointActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWaypointActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

