// Copyright Epic Games, Inc. All Rights Reserved.

#include "Assignment2HUD.h"
#include "Engine/Canvas.h"
#include "Engine/Texture2D.h"
#include "TextureResource.h"
#include "CanvasItem.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"

AAssignment2HUD::AAssignment2HUD()
{
	// Set the crosshair texture
	static ConstructorHelpers::FObjectFinder<UTexture2D> CrosshairTexObj(TEXT("/Game/FirstPerson/Textures/FirstPersonCrosshair"));
	if (CrosshairTexObj.Succeeded()) CrosshairTex = CrosshairTexObj.Object;

	PauseTitle = TEXT("PAUSED");

	// Get the font (in this case a default engine font)
	static ConstructorHelpers::FObjectFinder<UFont> PauseFontObj(TEXT("/Engine/EngineFonts/RobotoDistanceField"));
	if (PauseFontObj.Succeeded()) PauseFont = PauseFontObj.Object;
}


void AAssignment2HUD::DrawHUD()
{
	Super::DrawHUD();

	// Draw very simple crosshair

	// Find center of the Canvas
	const FVector2D Center(Canvas->ClipX * 0.5f, Canvas->ClipY * 0.5f);

	// Draw pause menu if paused
	if (UGameplayStatics::IsGamePaused(PlayerOwner->GetWorld())) {
		Canvas->DrawText(
			PauseFont,
			PauseTitle,
			Center.X - (PauseFont->GetStringSize(PauseTitle) / 2),
			Center.Y - (PauseFont->GetStringHeightSize(PauseTitle) / 2),
			1.f,
			1.f
		);
	}
	// Draw crosshair if not paused
	else {
		// Draw the crosshair
		// Offset by half the texture's dimensions so that the center of the texture aligns with the center of the Canvas
		FCanvasTileItem TileItem(FVector2D(Center.X, Center.Y + 20.0f), CrosshairTex->Resource, FLinearColor::White);
		TileItem.BlendMode = SE_BLEND_Translucent;
		Canvas->DrawItem(TileItem);
	}
}
