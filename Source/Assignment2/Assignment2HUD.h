// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "Assignment2HUD.generated.h"

class UGameplayStatics;

UCLASS()
class AAssignment2HUD : public AHUD
{
	GENERATED_BODY()

public:
	AAssignment2HUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D *CrosshairTex;

	/** Font asset pointer */
	class UFont *PauseFont;

	/** Pause menu title */
	const TCHAR *PauseTitle;
};

