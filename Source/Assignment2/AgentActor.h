// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AgentActor.generated.h"

class UStaticMeshComponent;
class UArrowComponent;
class KismetMathLibrary;
class Assignment2Character;
class UCameraComponent;
class UPawnMovementComponent;

UCLASS()
class ASSIGNMENT2_API AAgentActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAgentActor();

	// Agent's visible main body
	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent *VisibleComponentBody;

	// Visible arrow component
	UPROPERTY(VisibleAnywhere)
		UArrowComponent *VisibleComponentArrow;

	// Size of the agent - this also affects their speed
	UPROPERTY(EditAnywhere, Category = "Agent|AgentStatistics", meta = (ClampMin = "1.0", ClampMax = "3.0", UIMin = "1.0", UIMax = "3.0"))
		float Size;

	// Base speed of the agent - this is speed of the actor when size is 1
	UPROPERTY(EditAnywhere, Category = "Agent|AgentStatistics", meta = (ClampMin = "0.0", UIMin = "0.0"))
		float BaseSpeed;

	// The waypoint track the agent will use.
	UPROPERTY(EditAnywhere, Category = "Agent|AgentPathing", meta = (ClampMin = "1", ClampMax = "3", UIMin = "1", UIMax = "3"))
		int WaypointTrack;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// The speed of the agent - this is controlled by the size of the agent
	float FinalSpeed;

	// The index of the currently targeted waypoint vector
	int TargetedWaypointIndex;

	// Controls whether we are stopped and turning, or moving to the next waypoint
	bool bIsTurning;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Overlap event
	UFUNCTION()
	void BeginOverlap(
		UPrimitiveComponent *OverlappedComponent,
		AActor *OtherActor,
		UPrimitiveComponent *OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult &SweepResult
	);
};
