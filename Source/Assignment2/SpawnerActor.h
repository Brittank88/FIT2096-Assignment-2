// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpawnerActor.generated.h"

class UStaticMeshComponent;
class UGameplayStatics;

UCLASS()
class ASSIGNMENT2_API ASpawnerActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawnerActor();

	// The visual component
	UPROPERTY(VisibleAnywhere)
		UStaticMeshComponent *VisibleComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Interval between spawns
	UPROPERTY(EditAnywhere, meta = (ClampMin = "0.1", UIMin = "0.1"))
		float SpawnInterval;

	// Duration until first spawn
	UPROPERTY(EditAnywhere, meta = (ClampMin = "0", UIMin = "0"))
		int32 StartTime;

	// The number of actors to spawn
	UPROPERTY(EditAnywhere, meta = (ClampMin = "1", UIMin = "1"))
		int32 MaxSpawns;

	// Represents the class of the object to be spawned - must subclass AActor
	UPROPERTY(EditAnywhere, Category = "SpawnerActor|SpawnObject")
		TSubclassOf<AActor> SpawnObject;

	// Different Agent-specific properties that we will set for agents we are spawning
	UPROPERTY(EditAnywhere, Category = "SpawnerActor|SpawnObject|Agent Parameters|AgentPathing", meta = (ClampMin = "1", ClampMax = "3", UIMin = "1", UIMax = "3"))
		int32 AgentWaypointTrackIndex;
	UPROPERTY(EditAnywhere, Category = "SpawnerActor|SpawnObject|Agent Parameters|AgentStatistics", meta = (ClampMin = "1.0", ClampMax = "3.0", UIMin = "1.0", UIMax = "3.0"))
		float AgentSize;
	UPROPERTY(EditAnywhere, Category = "SpawnerActor|SpawnObject|Agent Parameters|AgentStatistics", meta = (ClampMin = "0.0", UIMin = "0.0"))
		float AgentBaseSpeed;

	// This is how we turn spawning on or off
	UPROPERTY(VisibleAnywhere)
	bool bStartSpawning = false;

	// This is how we track the time remaining until next spawn (after the first spawn)
	UPROPERTY(VisibleAnywhere)
	float SpawnCountdown;

	// The timerhandle for the timer that will pause for StartTime time before enabling StartSpawning
	UPROPERTY(VisibleAnywhere)
	FTimerHandle StartTimerHandle;

	// Helper function to advance our spawning timer
	void AdvanceTimer();

	// Tracks the current count of spawns
	UPROPERTY(VisibleAnywhere)
	int32 SpawnCount;
};
