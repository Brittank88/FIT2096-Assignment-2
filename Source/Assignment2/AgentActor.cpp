// Fill out your copyright notice in the Description page of Project Settings.


#include "AgentActor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Assignment2Character.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/PawnMovementComponent.h"

// This is an array of each "Track", which in itself is an array of FVectors representing world coordinates.
TArray<TArray<FVector>> WaypointTracks = {
	{
		FVector(-462.0, 161.0 , 100.0), FVector(-470.0, 379.0 , 100.0), FVector(265.0 , 391.0 , 100.0), FVector(276.0 , 632.0 , 100.0),
		FVector(534.0 , 637.0 , 100.0), FVector(534.0 , 859.0 , 100.0), FVector(-232.0, 868.0 , 100.0), FVector(-222.0, 1108.0, 100.0),
		FVector(35.0  , 1111.0, 100.0), FVector(38.0  , 1348.0, 100.0), FVector(261.0 , 1353.0, 100.0), FVector(266.0 , 1573.0, 100.0),
		FVector(27.0  , 1577.0, 100.0), FVector(31.0  , 1823.0, 100.0), FVector(545.0 , 1816.0, 100.0), FVector(541.0 , 1580.0, 100.0),
		FVector(266.0 , 1573.0, 100.0), FVector(261.0 , 1353.0, 100.0), FVector(508.0 , 1350.0, 100.0), FVector(501.0 , 1115.0, 100.0),
		FVector(-222.0, 1108.0, 100.0), FVector(-232.0, 868.0 , 100.0), FVector(534.0 , 859.0 , 100.0), FVector(534.0 , 637.0 , 100.0),
		FVector(276.0 , 632.0 , 100.0), FVector(265.0 , 391.0 , 100.0), FVector(531.0 , 389.0 , 100.0), FVector(537.0 , 161.0 , 100.0)
	},	// Waypoint track 1 (WaterWallRoom)
	{
		FVector(2368.0, 1503.0, 100.0), FVector(2340.0, 1899.0, 100.0), FVector(2090.0, 2139.0, 100.0), FVector(1338.0, 2193.0, 100.0),
		FVector(933.0 , 1765.0, 100.0), FVector(982.0 , 1180.0, 100.0), FVector(1301.0, 763.0 , 100.0), FVector(2375.0, 801.0 , 100.0)
	},	// Waypoint track 2 (Torus Room)
	{
		FVector(1811.0, -603.0, 100.0), FVector(1810.0, -110.0, 100.0), FVector(1325.0, -103.0, 100.0), FVector(1336.0, -598.0, 100.0)
	},	// Waypoint track 3 (RockRoom)
	{
		FVector(1317.0, -1437.0, 100.0), FVector(1275.0, -2314.0, 100.0), FVector(587.0, -2036.0, 100.0), FVector(572.0, -1445.0, 100.0)
	}	// Waypoint track 4 (PyramidRoom)
};

// Sets default values
AAgentActor::AAgentActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create root component
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	// Create the body visual component and attach it to the root component
	VisibleComponentBody = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BodyVisualRepresentation"));
	VisibleComponentBody->SetupAttachment(RootComponent);

	// Assign the NarrowCapsule static mesh to the body visible component
	static ConstructorHelpers::FObjectFinder<UStaticMesh> VisibleAsset(
		TEXT("/Game/StarterContent/Shapes/Shape_NarrowCapsule.Shape_NarrowCapsule")
	);
	if (VisibleAsset.Succeeded()) VisibleComponentBody->SetStaticMesh(VisibleAsset.Object);

	// Get the base material and apply this to our VisibleComponentBody
	// This will be replaced with a dynamic, tinted version later.
	static ConstructorHelpers::FObjectFinder<UMaterial> VisibleMaterial(
		TEXT("/Game/Assignment2/Materials/Unstable.Unstable")
	);
	if (VisibleMaterial.Succeeded()) VisibleComponentBody->SetMaterial(0, VisibleMaterial.Object);

	// Create the arrow visual component and attach it to the root component
	VisibleComponentArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("DirectionVisualRepresentation"));
	VisibleComponentArrow->SetHiddenInGame(false);
	VisibleComponentArrow->SetupAttachment(RootComponent);

	// We want the arrow to protrude from halfway up the actor model.
	FVector ArrowRelativeLocation = VisibleComponentArrow->GetRelativeLocation();
	ArrowRelativeLocation.Z = VisibleComponentBody->GetStaticMesh()->GetBounds().GetBox().GetSize().Z / 2;
	VisibleComponentArrow->SetRelativeLocation(ArrowRelativeLocation);

	// Initialise default property values
	Size = 1.f;
	BaseSpeed = 2.f;
	TargetedWaypointIndex = 1;	// Target the second waypoint, we will be spawning at the first waypoint
	bIsTurning = true;			// Immediately rotate to face the second waypoint
}

// Called when the game starts or when spawned
void AAgentActor::BeginPlay()
{
	Super::BeginPlay();

	// We would like to scale our entire agent according to our size statistic
	RootComponent->SetWorldScale3D(FVector(Size, Size, Size));

	// Calculate our actual movement speed from the base speed modifier and the size
	FinalSpeed = BaseSpeed * (1 / Size);
	
	// Use dynamic instance of material with tint based on size statistic.
	UMaterialInstanceDynamic *TintedVisibleMaterial = VisibleComponentBody->CreateAndSetMaterialInstanceDynamic(0);
	FLinearColor AgentTint = FLinearColor(255.f, 0.f, 0.f, 1.f);	// Red as a default linear colour, boosted to glow very brightly
	AgentTint = AgentTint.LinearRGBToHSV();							// Convert to HSV in preparation for hue adjustment
	AgentTint.R = FMath::GetMappedRangeValueClamped(FVector2D(1.f, 3.f), FVector2D(0.f, 270.f), Size);	// New max is 270 as 360 will just wrap back around to the same colour
	AgentTint = AgentTint.HSVToLinearRGB();							// Convert back to LinearRGB after applying hue adjustment
	TintedVisibleMaterial->SetVectorParameterValue("Tint", AgentTint.HSVToLinearRGB());
	TintedVisibleMaterial->SetVectorParameterValue("TintEmissive", AgentTint);

	// We need to know when we are overlapping our body component with the player
	VisibleComponentBody->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	VisibleComponentBody->OnComponentBeginOverlap.AddDynamic(this, &AAgentActor::BeginOverlap);
}

// Called every frame
void AAgentActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Get the current targeted waypoint
	FVector TargetedWaypoint = WaypointTracks[WaypointTrack][TargetedWaypointIndex];

	if (bIsTurning) {

		// Get the current angle and angle we need to rotate towards to be facing this waypoint
		FRotator WaypointLookAtRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), TargetedWaypoint);
		FRotator AgentCurrentRotation = GetActorRotation();

		// Remove any pitch and yaw, as we only want left / right rotations
		WaypointLookAtRotation.Pitch = 0;
		WaypointLookAtRotation.Roll = 0;
		AgentCurrentRotation.Pitch = 0;
		AgentCurrentRotation.Roll = 0;

		// Rotate towards the direction of the current targeted waypoint
		RootComponent->SetWorldRotation(FMath::Lerp(
			AgentCurrentRotation,
			WaypointLookAtRotation,
			DeltaTime * FinalSpeed
		));

		// If we're close enough to the correct angle, target the next waypoint
		bIsTurning = FMath::Abs(AgentCurrentRotation.Yaw - WaypointLookAtRotation.Yaw) >= 2.f;
	}
	else {
		// Move towards the current targeted waypoint
		RootComponent->SetWorldLocation(FMath::Lerp(
			GetActorLocation(), TargetedWaypoint, DeltaTime * FinalSpeed
		));

		// If we're close enough to the current targeted waypoint, target the next waypoint
		if (FVector::Dist(GetActorLocation(), TargetedWaypoint) < 2.f)
		{
			TargetedWaypointIndex++;
			if (TargetedWaypointIndex >= WaypointTracks[WaypointTrack].Num()) TargetedWaypointIndex = 0;

			// Begin turning to next waypoint
			bIsTurning = true;
		}
	}
}

// Called when an overlap is generated between this actor and some other actor
void AAgentActor::BeginOverlap (
	UPrimitiveComponent *OverlappedComponent,
	AActor *OtherActor,
	UPrimitiveComponent *OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult &SweepResult
) {
	// If the other actor was the player...
	if (OtherActor->IsA(AAssignment2Character::StaticClass())) {
		// Cast to the player class
		AAssignment2Character *PlayerActor = Cast<AAssignment2Character>(OtherActor);
		// Get the player's transform (stored by the player on begin)
		FTransform PlayerInitialLocationAndRotation = PlayerActor->InitialLocationAndRotation;
		// Set the transform to the spawning transform
		PlayerActor->SetActorTransform(PlayerInitialLocationAndRotation);
		// Set the controller rotation to the spawning camera angle
		PlayerActor->GetController()->ClientSetRotation(PlayerInitialLocationAndRotation.Rotator());
		// Zero out the velocity, to avoid moving a bit after respawning (in cases such as being respawned whilst walking)
		PlayerActor->GetMovementComponent()->Velocity = FVector::ZeroVector;
	}
}